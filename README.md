Description
------------

This repository is used as the final stage in the release process of the `eos-xrootd` package. After having created the xrootd tarball from the [eos-xrootd-code](https://gitlab.cern.ch/eos/eos-xrootd-code) repository, this needs to be copied over to this repository in the `SOURCES` directory.
The next step is to update the `SPEC/eos-xrootd.spec` file so that the version from the SPEC file matches the version of the newly created xrootd tarball. For example, for the eos-xrootd 5.5.8 release, one needs to make the following changes to the `eos-xrootd.spec` file:
```
Version:        5.6.6
Release:        2%{?dist}%{?_with_asan:.asan}%{?_with_tsan:.tsan}
```
Pay attention to the first number in the Release ! In that example, the eos-xrootd version will be 5.6.6-2. Modify the release version to 1 to have 5.6.6-1

The newly included tarball together with the SPEC changes should be added as a new commit and pushed to the upstream repository. At this point, the release process will happen automatically in the CI pipeline of the `eos-xrootd` project in GitLab. The last stage of the pipeline corresponds to the release publishing. Note that by default, new `eos-xrootd` releases are not pushed automatically in Koji and this is a manual step. The reason is that sometimes a new release needs to be validated before being pushed to Koji as this platform does not support overwriting packages with the same version. The publishing to Koji is an important step for releasing the eos client in Koji as this relies on the presence of the `eos-xrootd` packages also in Koji.
