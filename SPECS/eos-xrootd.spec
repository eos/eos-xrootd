
#-------------------------------------------------------------------------------
# The eos-xrootd package provides a series of libraries might clash with the
# vanilla xrootd packages. To avoid such conflicts, we disable RPMS's automatic
# dependency processing when it  comes to the list of capabilities the current
# package provides.
#-------------------------------------------------------------------------------
Autoprov: 0
%global __requires_exclude ^libXrd*
%define _prefix /opt/eos/xrootd/
%define _unpackaged_files_terminate_build 0

Summary:        EOS XRootD version
Name:           eos-xrootd
Version:        5.7.3
Release:        1%{?dist}%{?_with_asan:.asan}%{?_with_tsan:.tsan}
License:        BSD
URL:            https://github.com/xrootd/xrootd
Source:         http://xrootd.web.cern.ch/download/v%{version}/xrootd-%{version}.tar.gz

# For asan/tsan builds we need devtoolset-9
%if %{?_with_asan:1}%{!?_with_asan:0} || %{?_with_tsan:1}%{!?_with_tsan:0}
%define devtoolset devtoolset-9
%else
%define devtoolset devtoolset-8
%endif

%if 0%{?rhel} == 7
BuildRequires: %{devtoolset}-gcc-c++
BuildRequires: cmake3
%define cmake cmake3
%else
BuildRequires: cmake
%define cmake cmake
%endif

BuildRequires: krb5-devel
BuildRequires: readline-devel
BuildRequires: fuse-devel
BuildRequires: libxml2-devel
BuildRequires: krb5-devel
BuildRequires: zlib-devel
BuildRequires: ncurses-devel
BuildRequires: libuuid-devel
BuildRequires: json-c-devel
BuildRequires: libcurl-devel
BuildRequires: voms-devel
BuildRequires: libmacaroons-devel
BuildRequires: scitokens-cpp-devel
BuildRequires: openssl-devel
BuildRequires: python3-devel
BuildRequires: python3-pip

%if %{?_with_asan:1}%{!?_with_asan:0}
%if 0%{?rhel} == 7
BuildRequires: libasan5, %{devtoolset}-libasan-devel
Requires: libasan5
%else
BuildRequires: libasan
Requires: libasan
%endif
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
%if 0%{?rhel} == 7
BuildRequires: libtsan, %{devtoolset}-libtsan-devel
Requires: libtsan
%else
BuildRequires: libtsan
Requires: libtsan
%endif
%endif

%description
An xrootd installation with RPATH set to /opt/eos/xrootd/

%prep
%setup -n xrootd-%{version}

%build
%if %{?_with_asan:1}%{!?_with_asan:0}
export CXXFLAGS='-fsanitize=address'
%endif

%if %{?_with_tsan:1}%{!?_with_tsan:0}
export CXXFLAGS='-fsanitize=thread'
%endif

# Enable devtoolset for builds on CentOS7
%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

mkdir build
pushd build
%{cmake} ../ -DCMAKE_BUILD_TYPE=RelWithDebInfo         \
             -DCMAKE_INSTALL_PREFIX=%{_prefix}         \
             -DCMAKE_INSTALL_RPATH=%{_prefix}/lib64/   \
             -DCMAKE_SKIP_BUILD_RPATH=false            \
             -DCMAKE_BUILD_WITH_INSTALL_RPATH=false    \
             -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=true  \
             -DXRD_PYTHON_REQ_VERSION=3                \
             -DPython_FIND_UNVERSIONED_NAMES=FIRST     \
             -DXRootD_VERSION_STRING=%{version}        \
             -DPIP_OPTIONS=--verbose
make %{?_smp_mflags}
popd

%install
# We have to activate devtoolset in install section as well, since the generation
# of debuginfo happens here. "make install" will still remember to use devtoolset,
# but find-debuginfo.sh will not, resulting in a debuginfo package that is corrupted
# and may cause GDB to crash.
%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

export QA_RPATHS=3
rm -rf $RPM_BUILD_ROOT
pushd build
make install DESTDIR=$RPM_BUILD_ROOT
popd

%clean
rm -rf %{buildroot}

%files
%{_prefix}/
